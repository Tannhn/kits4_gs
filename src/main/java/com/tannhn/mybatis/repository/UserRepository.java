package com.tannhn.mybatis.repository;

import com.tannhn.mybatis.entity.UserEntity;

public interface UserRepository extends BaseRepository<UserEntity> {

}
