package com.tannhn.mybatis.repository;

import com.tannhn.mybatis.entity.BlogEntity;

public interface BlogRepository extends BaseRepository<BlogEntity> {

}
