package com.tannhn.mybatis.repository.impl;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tannhn.mybatis.entity.UserEntity;
import com.tannhn.mybatis.repository.UserRepository;

@Repository
public class UserRepositoryMybatis implements UserRepository {
	
	@Autowired
	private SqlSessionTemplate session;
	
	@Override
	public void insert(UserEntity vo) {
		// TODO Auto-generated method stub

	}

	@Override
	public void update(UserEntity vo) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(UserEntity vo) {
		// TODO Auto-generated method stub

	}

	@Override
	public UserEntity get(UserEntity vo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<UserEntity> list(UserEntity vo) {
		return session.selectList("User.selectList", vo);
	}

}
