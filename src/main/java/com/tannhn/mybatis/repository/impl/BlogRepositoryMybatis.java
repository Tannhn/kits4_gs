package com.tannhn.mybatis.repository.impl;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.tannhn.mybatis.entity.BlogEntity;
import com.tannhn.mybatis.repository.BlogRepository;

@Repository
public class BlogRepositoryMybatis implements BlogRepository{
	
	@Autowired
	private SqlSessionTemplate session;
	
	@Override
	public void insert(BlogEntity vo) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(BlogEntity vo) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(BlogEntity vo) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public BlogEntity get(BlogEntity vo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<BlogEntity> list(BlogEntity vo) {
		return session.selectList("Blog.selectList", null);
	}
	
}
