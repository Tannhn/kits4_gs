package com.tannhn.mybatis.entity;

import java.util.Set;

import javax.annotation.processing.Generated;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BlogEntity {
	
	private int id;
	
	private String title;
	
	private UserEntity user;
	
	private Set<TagEntity> tags;
}


