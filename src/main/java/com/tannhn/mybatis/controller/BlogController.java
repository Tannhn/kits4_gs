package com.tannhn.mybatis.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tannhn.mybatis.entity.BlogEntity;
import com.tannhn.mybatis.service.BlogService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/v1/blog")
public class BlogController {
	
	private BlogService blogService;
	
	@Autowired
	public BlogController(BlogService blogService) {
		this.blogService = blogService;
	}
	
	@GetMapping("/list")
	public ResponseEntity<?> listBlog() {
		log.info("listBlog");
		
		List<BlogEntity> list = blogService.listBlog(null);
		
		return new ResponseEntity<List<BlogEntity>>(list, HttpStatus.OK);
	}
}
