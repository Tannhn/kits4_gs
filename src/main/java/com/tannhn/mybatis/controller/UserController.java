package com.tannhn.mybatis.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tannhn.mybatis.entity.UserEntity;
import com.tannhn.mybatis.service.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("api/v1/user")
public class UserController {
	//public static final Logger log = LoggerFactory.getLogger(UserController.class);
	
	private UserService userService;
	
	@Autowired
	public UserController(UserService userService) {
		this.userService = userService;
	}
	
	@GetMapping("/list")
	public ResponseEntity<?> getUserList() {
		log.info("getUserList");
		
		List<UserEntity> list = userService.listUser(null);
		
		return new ResponseEntity<List<UserEntity>>(list, HttpStatus.OK);
	}
	
	@GetMapping("/list-json")
	public String getUserListJson() {
		log.info("getUserList");
		
		List<UserEntity> list = userService.listUser(null);
		
		ObjectMapper objectMapper = new ObjectMapper();
		String json= "";
		try {
			json = objectMapper.writeValueAsString(list);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return json;
	}
}
