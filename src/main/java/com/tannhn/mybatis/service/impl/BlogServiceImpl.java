package com.tannhn.mybatis.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tannhn.mybatis.entity.BlogEntity;
import com.tannhn.mybatis.repository.BlogRepository;
import com.tannhn.mybatis.service.BlogService;

@Service
public class BlogServiceImpl implements BlogService {
	
	private BlogRepository blogRepo;
	
	@Autowired
	public BlogServiceImpl(BlogRepository blogRepo) {
		this.blogRepo = blogRepo;
	}
	
	@Override
	public void insertBlog(BlogEntity blog) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateBlog(BlogEntity blog) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteBlog(BlogEntity blog) {
		// TODO Auto-generated method stub

	}

	@Override
	public BlogEntity getBlog(BlogEntity blog) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<BlogEntity> listBlog(BlogEntity blog) {
		return blogRepo.list(blog);
	}

}
