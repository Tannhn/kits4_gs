package com.tannhn.mybatis.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tannhn.mybatis.entity.UserEntity;
import com.tannhn.mybatis.repository.UserRepository;

@Service
public class UserService {
	
	@Autowired
	private UserRepository	userRepo;

	public void insertUser(UserEntity vo) {
		// vaildation, mispatch, wrong
		userRepo.insert(vo);
	}
	public void deleteUser(UserEntity vo) {
		userRepo.delete(vo);
	}
	public void updateUser(UserEntity vo) {
		userRepo.update(vo);
	}
	public UserEntity getUser(UserEntity vo) {
		return userRepo.get(vo);
	}
	public List<UserEntity> listUser(UserEntity vo) {
		return userRepo.list(vo);
	}
}
