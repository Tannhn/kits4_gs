package com.tannhn.mybatis.service;

import java.util.List;

import com.tannhn.mybatis.entity.BlogEntity;

public interface BlogService {
	
	void insertBlog(BlogEntity blog);
	
	void updateBlog(BlogEntity blog);
	
	void deleteBlog(BlogEntity blog);
	
	BlogEntity getBlog(BlogEntity blog);
	
	List<BlogEntity> listBlog(BlogEntity blog);
}
